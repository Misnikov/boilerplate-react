import React from 'react';
import ReactDOM from 'react-dom';


import 'normalize.css';
import './styles/scss/main.scss';


const rootEl = document.getElementById('root');
const app = <div>React App</div>

ReactDOM.render(app, rootEl);



